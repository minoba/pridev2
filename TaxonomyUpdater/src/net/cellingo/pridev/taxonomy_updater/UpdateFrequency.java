/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cellingo.pridev.taxonomy_updater;

/**
 *
 * @author Gebruiker
 */
public enum UpdateFrequency {
    WEEK("Week", 7),
    ONE_MONTH("1 Month", 30),
    THREE_MONTH("3 Months", 90),
    SIX_MONTH("6 Months", 180),
    YEAR("Year", 365),
    NEVER("Never", Integer.MAX_VALUE);
    
    private final String type;
    private final int days;
    private int rank;
    
    private UpdateFrequency(String type, int days){
        this.type = type;
        this.days = days;
        this.rank = rank;
    }
    
    @Override
    public String toString(){
        return type;
    }
    
    /**
     * returns the update frequency in days
     * @return days
     */
    public int getUpdateFrequency(){
        return days;
    }
    
}
