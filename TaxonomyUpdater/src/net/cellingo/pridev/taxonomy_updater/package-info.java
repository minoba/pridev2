/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@OptionsPanelController.ContainerRegistration(id = "Taxonomy", categoryName = "#OptionsCategory_Name_Taxonomy", iconBase = "net/cellingo/pridev/taxonomy_updater/tree_mono32.png", keywords = "#OptionsCategory_Keywords_Taxonomy", keywordsCategory = "Taxonomy")
@NbBundle.Messages(value = {"OptionsCategory_Name_Taxonomy=Taxonomy", "OptionsCategory_Keywords_Taxonomy=ncbi archive taxonomy"})
package net.cellingo.pridev.taxonomy_updater;

import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;
