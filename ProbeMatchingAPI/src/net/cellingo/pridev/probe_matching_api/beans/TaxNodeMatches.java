/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cellingo.pridev.probe_matching_api.beans;

import net.cellingo.pridev.probe_matcher_api.MatchStatisticSelection;
import java.util.ArrayList;
import java.util.List;
import net.cellingo.pridev.probe_matcher_api.AnalysisMode;
import net.cellingo.pridev.probe_matcher_api.TaxNodeMatchResults;

/**
 *
 * @author Gebruiker
 */
public class TaxNodeMatches {
    private List<TaxNodeMatchResults> taxNodeMatches = new ArrayList<TaxNodeMatchResults>();
    private MatchStatisticSelection matchStatistic;
    private AnalysisMode analysisMode;
    private int taxID;

    public TaxNodeMatches(MatchStatisticSelection matchStatistic) {
        this.matchStatistic = matchStatistic;
    }

    public MatchStatisticSelection getMatchStatistic() {
        return matchStatistic;
    }

    public void setMatchStatistic(MatchStatisticSelection matchStatistic) {
        this.matchStatistic = matchStatistic;
    }

    public AnalysisMode getAnalysisMode() {
        return analysisMode;
    }

    public void setAnalysisMode(AnalysisMode analysisMode) {
        this.analysisMode = analysisMode;
    }

    
    public int getTaxID() {
        return taxID;
    }

    public void setTaxID(int taxID) {
        this.taxID = taxID;
    }

    
    public MatchStatisticSelection getMatchStatisticSelection() {
        return matchStatistic;
    }

    public List<TaxNodeMatchResults> getTaxNodeMatches() {
        return taxNodeMatches;
    }
    
    public void addTaxNodeMatch( TaxNodeMatchResults taxNodeMatch) {
        taxNodeMatches.add(taxNodeMatch);
    }

    public void setTaxNodeMatches(List<TaxNodeMatchResults> taxNodeMatches) {
        this.taxNodeMatches = taxNodeMatches;
    }
    
    
}
