/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cellingo.pridev.probe_matching_api.beans;

import java.util.HashMap;

/**
 *
 * @author Gebruiker
 */
public class FragmentSizes {
    private int taxID;
    private HashMap<Integer, Integer> truePositives = new HashMap<Integer, Integer>();
    private HashMap<Integer, Integer> falsePositives = new HashMap<Integer, Integer>();

    public FragmentSizes(int taxID) {
        this.taxID = taxID;
    }

    public HashMap<Integer, Integer> getTruePositivesFragmentSizes() {
        return truePositives;
    }

    public void addTruePositiveFragmentSize(int size, int count){
        this.truePositives.put(size, count);
    }
    
    public void setTruePositiveFragmentSizes(HashMap<Integer, Integer> truePositives) {
        this.truePositives = truePositives;
    }

    public HashMap<Integer, Integer> getFalsePositiveFragmentSizes() {
        return falsePositives;
    }

    public void addFalsePositiveFragmentSize(int size, int count){
        this.falsePositives.put(size, count);
    }

    public void setFalsePositiveFragmentSizes(HashMap<Integer, Integer> falsePositives) {
        this.falsePositives = falsePositives;
    }

    public int getTaxID() {
        return taxID;
    }
    
    
}
