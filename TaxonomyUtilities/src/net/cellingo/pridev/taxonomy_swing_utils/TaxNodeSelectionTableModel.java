/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.taxonomy_swing_utils;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import nl.bioinf.noback.taxonomy.model.TaxNode;
import nl.bioinf.noback.taxonomy.model.TaxTree;

/**
 *
 * @author M.A Noback (michiel@cellingo.net)
 * 
 */
public class TaxNodeSelectionTableModel extends AbstractTableModel {
    private TaxTree treeDummy = new TaxTree();
    private final List<TaxNode> taxNodes;
    private final List<Boolean> selectedIndices;

    /**
     * construct with a list of TaxNode objects
     *
     * @param taxNodes
     */
    public TaxNodeSelectionTableModel(List<TaxNode> taxNodes) {
        this.taxNodes = taxNodes;
        selectedIndices = new ArrayList<Boolean>();
        for (int i = 0; i < taxNodes.size(); i++) {
            selectedIndices.add(Boolean.FALSE);
        }
    }

    /**
     * add a new list of taxnodes to the table
     *
     * @param newTaxNodes
     */
    public void addtaxNodes(List<TaxNode> newTaxNodes) {
        this.taxNodes.addAll(newTaxNodes);

        for (int i = 0; i < newTaxNodes.size(); i++) {
            selectedIndices.add(Boolean.FALSE);
        }
        fireTableDataChanged();
    }

    /**
     * clears the table
     */
    public void clearData() {
        this.taxNodes.clear();
        this.selectedIndices.clear();
        fireTableDataChanged();
    }

    /**
     * select all rows
     */
    public void selectAll() {
        for (int i = 0; i < this.selectedIndices.size(); i++) {
            this.selectedIndices.set(i, Boolean.TRUE);
        }
        fireTableDataChanged();
    }

    /**
     * unselect all rows
     */
    public void unselectAll() {
        for (int i = 0; i < this.selectedIndices.size(); i++) {
            this.selectedIndices.set(i, Boolean.TRUE);
        }
        fireTableDataChanged();
   }

    @Override
    public int getRowCount() {
        return taxNodes.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Select";
            case 1:
                return "Tax ID";
            case 2:
                return "Scientific name";
            case 3:
                return "Level";
            case 4:
                return "# Species";
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Boolean.class;
            case 1:
                return Integer.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return Integer.class;
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return selectedIndices.get(rowIndex);
            case 1:
                return taxNodes.get(rowIndex).getTaxID();
            case 2:
                return taxNodes.get(rowIndex).getScientificName();
            case 3:
                return taxNodes.get(rowIndex).getRank().toString();
            case 4:
                return treeDummy.getSpeciesChildren(taxNodes.get(rowIndex)).size();//getChildNumber();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        //super.setValueAt(aValue, rowIndex, columnIndex);
        if (columnIndex == 0) {
            this.selectedIndices.set(rowIndex, (Boolean) aValue);
        }
        fireTableDataChanged();
    }

    /**
     * returns the list of selected nodes from this tablemodel
     *
     * @return selectedNodes
     */
    public List<TaxNode> getSelectedNodes() {
        List<TaxNode> selectedNodes = new ArrayList<TaxNode>();
        for (int i = 0; i < this.selectedIndices.size(); i++) {
            if (this.selectedIndices.get(i)) {
                selectedNodes.add(this.taxNodes.get(i));
            }
        }
        return selectedNodes;
    }
}