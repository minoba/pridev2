/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.taxonomy_loader_action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import net.cellingo.pridev.taxonomy_archive.NcbiTaxonomyArchiveWrapper;
import net.cellingo.pridev.utilities.CentralLookup;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "File",
        id = "net.cellingo.pridev.taxonomy_loader_action.SetNcbiTaxonomyArchiveAction")
@ActionRegistration(
        iconBase = "net/cellingo/pridev/taxonomy_loader_action/tree_mono.png",
        displayName = "#CTL_SetNcbiTaxonomyArchiveAction")
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 1300, separatorAfter = 1350),
    @ActionReference(path = "Toolbars/File", position = 100),
    @ActionReference(path = "Shortcuts", name = "D-N D-T")
})
@Messages("CTL_SetNcbiTaxonomyArchiveAction=Set NCBI taxonomy archive")
public final class SetNcbiTaxonomyArchiveAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fc = new JFileChooser();
        String ttText = "Specify location of the NCBI taxonomy zip archive";
        String dialogTitle = "Set NCBI taxonomy zip archive";
        fc.setToolTipText(ttText);
        fc.setDialogTitle(dialogTitle);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.addChoosableFileFilter(new FileFilter() {

            @Override
            public boolean accept(File f) {
               if (f.isDirectory()) {
                   return true;
               } else {
                   return f.getName().toLowerCase().endsWith(".pdf");
               }
            }

            @Override
            public String getDescription(){
                return "ZIP archive (*.zip)";
            }
        });
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == 0) {
            File ncbiTaxonomyArchive =  fc.getSelectedFile();
            NcbiTaxonomyArchiveWrapper ncbiWrapper = new NcbiTaxonomyArchiveWrapper(ncbiTaxonomyArchive);
            //System.out.println("ncbi arch=" + ncbiTaxonomyArchive);
            CentralLookup.getDefault().add(ncbiWrapper);
        }
    }
}
