/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.taxonomy_archive;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipException;
import nl.bioinf.noback.taxonomy.io.NcbiTaxonomyArchiveReader;
import nl.bioinf.noback.taxonomy.model.CorruptedLineageException;
import nl.bioinf.noback.taxonomy.model.TaxTree;
import org.openide.util.Exceptions;
/**
 *
 * @author Gebruiker
 */
public class NcbiTaxonomyArchiveWrapper {
        /**
     * the NCBI taxonomy zip archive file
     */
    private static File archiveFile;
    private static TaxTree taxTree;
    
    public NcbiTaxonomyArchiveWrapper(File archiveFile){
        NcbiTaxonomyArchiveWrapper.archiveFile = archiveFile;
    }

    public static File getArchiveFile() {
        return archiveFile;
    }

    public static void loadArchive() {
        //System.out.println("loading archive from zip");
        NcbiTaxonomyArchiveReader reader = new NcbiTaxonomyArchiveReader();
        try {
            NcbiTaxonomyArchiveWrapper.taxTree = reader.readZip(archiveFile);
            //System.out.println("taxTree=" + taxTree);
        } catch (ZipException ex) {
            System.out.println("zip exception");
            Exceptions.printStackTrace(ex);
        } catch (IOException ex) {
            System.out.println("IO EX");
            Exceptions.printStackTrace(ex);
        } catch (CorruptedLineageException ex) {
            System.out.println("Corr LE");
            Exceptions.printStackTrace(ex);
        }
    }
    
    /**
     * returns the current NCBI taxonomy tree of the application
     * @return NCBI TaxTree
     */
    public static TaxTree getDefault(){
        return taxTree;
    }
    

}
