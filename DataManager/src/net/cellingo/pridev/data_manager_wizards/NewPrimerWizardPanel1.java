/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_wizards;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class NewPrimerWizardPanel1 implements WizardDescriptor.Panel<WizardDescriptor>, PropertyChangeListener {
    private boolean isValid = false;
    private final EventListenerList listeners = new EventListenerList();
    private WizardDescriptor wiz = null;
    
    
    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private NewPrimerVisualPanel1 component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public NewPrimerVisualPanel1 getComponent() {
        if (component == null) {
            component = new NewPrimerVisualPanel1();
            //component.addPropertyChangeListener(this);
        }
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        //return true;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
        //
        return checkValidity();
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        listeners.add(ChangeListener.class, l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        listeners.remove(ChangeListener.class, l);
    }
    
    protected final void fireChangeEvent(Object source, boolean oldState, boolean newState){
        if(oldState != newState){
            ChangeEvent ev = new ChangeEvent(source);
            for (ChangeListener listener : listeners.getListeners(ChangeListener.class)){
                listener.stateChanged(ev);
            }
        }
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        this.wiz = wiz;
        getComponent().addPropertyChangeListener(this);
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        wiz.putProperty(NewPrimerVisualPanel1.PROP_PRIMER_NAME, getComponent().getPrimerName());
        wiz.putProperty(NewPrimerVisualPanel1.PROP_FILE_LOCATION, getComponent().getFolder());
        wiz.putProperty(NewPrimerVisualPanel1.PROP_SEQUENCE, getComponent().getSequence());
        wiz.putProperty(NewPrimerVisualPanel1.PROP_STRAND, getComponent().getStrand());
        wiz.putProperty(NewPrimerVisualPanel1.PROP_XREF, getComponent().getXref());
        wiz.putProperty(NewPrimerVisualPanel1.PROP_DESCRIPTION, getComponent().getDescription());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        boolean oldState = isValid;
        isValid = checkValidity();
        fireChangeEvent(this, oldState, isValid);
    }
    
    
    
    private boolean checkValidity(){
        if(getComponent().getPrimerName().length() < 6){
            setErrorMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.filename"));
            return false;
        }
        
        if(getComponent().getFolder().length() == 0){
            setErrorMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.path"));
            return false;
        }
        File folder = new File(getComponent().getFolder());
        File primerFile;
        if(folder.getAbsolutePath().endsWith(File.separator)){
            primerFile = new File(folder.getAbsolutePath() + getComponent().getPrimerName() + ".pri");
        }else{
            primerFile = new File(folder.getAbsolutePath() + File.separator + getComponent().getPrimerName() + ".pri");
        }
        
        //check validity of path
        try{
            primerFile.getCanonicalPath();
        }
        catch(IOException e){
            setErrorMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.path"));
            return false;
        }
        
        if(primerFile.exists()){
            setErrorMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.file_exists"));
            return false;
        }
        
        String sequence = getComponent().getSequence().toUpperCase();
        String pattern = "([GATCWSRYKMBDHVN]){10,}";

        Pattern p = Pattern.compile(pattern);
        if( ! p.matcher(sequence).matches()) {
            setErrorMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.illegal_sequence"));
            return false;
        }
        
//        String strand = getComponent().getStrand();
//        System.out.println("strand="+ strand);
//        
//        if(! (getComponent().getStrand().equals("Forward strand") ||
//                getComponent().getStrand().equals("Reverse strand") ) ){
//            setErrorMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.choose_strand"));
//            return false;
//            
//        }
        
        setMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.input_valid"));
        return true;
    }
    
    private void setMessage(String message){
        wiz.getNotificationLineSupport().setInformationMessage(message);
    }
    private void setErrorMessage(String message){
        wiz.getNotificationLineSupport().setErrorMessage(message);
    }
    
}
