/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_wizards;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class NewPrimerWizardPanel2 implements WizardDescriptor.Panel<WizardDescriptor>, PropertyChangeListener {

    private boolean isValid = false;
    private final EventListenerList listeners = new EventListenerList();
    private WizardDescriptor wiz = null;
    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private NewPrimerVisualPanel2 component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public NewPrimerVisualPanel2 getComponent() {
        if (component == null) {
            component = new NewPrimerVisualPanel2();
        }
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid() {

        return checkValidity();
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        listeners.add(ChangeListener.class, l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        listeners.remove(ChangeListener.class, l);
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        this.wiz = wiz;
        getComponent().addPropertyChangeListener(this);
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        wiz.putProperty(NewPrimerVisualPanel2.PROP_SELECTED_TARGETS, getComponent().getSelectedTargets());
    }

    private boolean checkValidity() {
        if (getComponent().getSelectedTargets().isEmpty()) {
            setErrorMessage(NbBundle.getMessage(NewPrimerWizardPanel2.class, "NewPrimerWizardPanel2.errormessage.no_selected_nodes"));
            return false;
        }
        setMessage(NbBundle.getMessage(NewPrimerWizardPanel1.class, "NewPrimerWizardPanel1.errormessage.input_valid"));
        return true;
    }

    private void setMessage(String message) {
        wiz.getNotificationLineSupport().setInformationMessage(message);
    }

    private void setErrorMessage(String message) {
        wiz.getNotificationLineSupport().setErrorMessage(message);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        boolean oldState = isValid;
        isValid = checkValidity();
        fireChangeEvent(this, oldState, isValid);
    }

    protected final void fireChangeEvent(Object source, boolean oldState, boolean newState) {
        //System.out.println("checking status: " + source + "/" + oldState + "/" + newState);
        if (oldState != newState) {
            ChangeEvent ev = new ChangeEvent(source);
            for (ChangeListener listener : listeners.getListeners(ChangeListener.class)) {
                listener.stateChanged(ev);
            }
        }
    }
}
