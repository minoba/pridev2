/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_wizards;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import net.cellingo.pridev.data_manager.file_type_primer.PriDataObject;
import nl.bioinf.noback.taxonomy.model.TaxNode;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.loaders.DataObjectExistsException;
import org.openide.util.Exceptions;

// An example action demonstrating how the wizard could be called from within
// your code. You can move the code below wherever you need, or register an action:
@ActionID(category="File", id="net.cellingo.pridev.data_manager_wizards.NewPrimerWizardAction")
@ActionRegistration(
    iconBase = "net/cellingo/pridev/data_manager_wizards/probe.png",
    displayName="Create new probe/primer")
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 1200),
    @ActionReference(path = "Toolbars/File", position = 300),
    @ActionReference(path = "Shortcuts", name = "D-F")
})
public final class NewPrimerWizardAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        List<WizardDescriptor.Panel<WizardDescriptor>> panels = new ArrayList<WizardDescriptor.Panel<WizardDescriptor>>();
        panels.add(new NewPrimerWizardPanel1());
        panels.add(new NewPrimerWizardPanel2());
        String[] steps = new String[panels.size()];
        for (int i = 0; i < panels.size(); i++) {
            Component c = panels.get(i).getComponent();
            // Default step name to component name of panel.
            steps[i] = c.getName();
            if (c instanceof JComponent) { // assume Swing components
                JComponent jc = (JComponent) c;
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_SELECTED_INDEX, i);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, steps);
                jc.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, true);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DISPLAYED, true);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_NUMBERED, true);
            }
        }
        WizardDescriptor wiz = new WizardDescriptor(new WizardDescriptor.ArrayIterator<WizardDescriptor>(panels));
        // {0} will be replaced by WizardDesriptor.Panel.getComponent().getName()
        wiz.setTitleFormat(new MessageFormat("{0}"));
        String title = org.openide.util.NbBundle.getMessage(NewPrimerWizardAction.class, "NewWorkspaceAction.title.text");
        wiz.setTitle(title);
        if (DialogDisplayer.getDefault().notify(wiz) == WizardDescriptor.FINISH_OPTION) {
            try {
                //PriDataObject pdo = new PriDataObject(null, PriDataObject.Factory);
                
                 Map<String,Object> props = wiz.getProperties();
                 @SuppressWarnings("unchecked")
                 List<TaxNode> tnl = (List<TaxNode>)props.get(NewPrimerVisualPanel2.PROP_SELECTED_TARGETS);
                 PriDataObject pda = PriDataObject.createNew(
                         (String)props.get(NewPrimerVisualPanel1.PROP_PRIMER_NAME), 
                         (String)props.get(NewPrimerVisualPanel1.PROP_FILE_LOCATION), 
                         (String)props.get(NewPrimerVisualPanel1.PROP_SEQUENCE), 
                         (String)props.get(NewPrimerVisualPanel1.PROP_STRAND), 
                         (String)props.get(NewPrimerVisualPanel1.PROP_DESCRIPTION), 
                         (String)props.get(NewPrimerVisualPanel1.PROP_XREF),
                         tnl);
                 //TODO now it needs to be published
            } catch (DataObjectExistsException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }
}
