/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_actions;

import java.io.File;
import java.util.Collection;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import net.cellingo.pridev.probe_matcher_api.ProbeMatcher;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author Gebruiker
 */
public class ProbeMatcherActionVisualPanel1 extends JPanel implements DocumentListener {
    public static final String PROP_ALGORITHM = "matchingAlgotithm";
    public static final String PROP_SEQUENCE_SET = "sequenceSet";
    public static final String PROP_MAX_MISMATCHES_OVERALL = "mismatchesOverall";
    public static final String PROP_MAX_MISMATCHES_3_PRIME = "mismatches3Prime";
    public static final String PROP_ALSO_REVERSE_COMPLEMENT = "alsoReverseComplement";
    public static final String PROP_OUTPUT_FOLDER = "outputFolder";
    
    
    private ComboBoxModel<ProbeMatcher> algorithmComboModel;
    /**
     * Creates new form ProbeMatcherActionVisualPanel1
     */
    public ProbeMatcherActionVisualPanel1() {
        Collection<? extends ProbeMatcher> matchers = Lookup.getDefault().lookupAll(ProbeMatcher.class);
        ProbeMatcher[] a = matchers.toArray(new ProbeMatcher[matchers.size()]);
        algorithmComboModel = new DefaultComboBoxModel<ProbeMatcher>(a);
        initComponents();
        
        this.sequenceCollectionTextField.getDocument().addDocumentListener(this);
        this.outputFolderTextField.getDocument().addDocumentListener(this);
    }
    
    @Override
    public String getName() {
        return NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.name");
    }

    public ProbeMatcher getProbeMatcherAlgorithm(){
        return (ProbeMatcher)this.algorithmComboModel.getSelectedItem();
    }
    
    public String getSequenceSetFile(){
        return this.sequenceCollectionTextField.getText();
    }
    
    public int getMaximumMismatchesOverall(){
        return (Integer)this.overallGapNumberSelectionCombo.getSelectedItem();
    }
    
    public int getMaximumMismatches3PrimeEnd(){
        return (Integer)this.threePrimeGapNumberSelectionCombo.getSelectedItem();
    }
    
    public boolean getAlsoReverseComplement(){
        return (Boolean)this.alsoReverseComplementCheckBox1.isSelected();
    }
    
    public String getOutputFolder(){
        return this.outputFolderTextField.getText();
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        probeNameLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<ProbeMatcher>();
        jLabel2 = new javax.swing.JLabel();
        sequenceCollectionTextField = new javax.swing.JTextField();
        selectSequenceCollectionButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        overallGapNumberSelectionCombo = new javax.swing.JComboBox<Integer>();
        jLabel5 = new javax.swing.JLabel();
        threePrimeGapNumberSelectionCombo = new javax.swing.JComboBox<Integer>();
        jLabel6 = new javax.swing.JLabel();
        alsoReverseComplementCheckBox1 = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        outputFolderTextField = new javax.swing.JTextField();
        selectOutputFolderButton = new javax.swing.JButton();

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jLabel1.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(probeNameLabel, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.probeNameLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jLabel3.text")); // NOI18N

        jComboBox1.setModel(algorithmComboModel);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jLabel2.text")); // NOI18N

        sequenceCollectionTextField.setText(org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.sequenceCollectionTextField.text")); // NOI18N
        sequenceCollectionTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sequenceCollectionTextFieldActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(selectSequenceCollectionButton, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.selectSequenceCollectionButton.text")); // NOI18N
        selectSequenceCollectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectSequenceCollectionButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jLabel4.text")); // NOI18N

        overallGapNumberSelectionCombo.setModel(new javax.swing.DefaultComboBoxModel<Integer>(new Integer[] { 0, 1, 2, 3, 4, 5 }));

        org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jLabel5.text")); // NOI18N

        threePrimeGapNumberSelectionCombo.setModel(new javax.swing.DefaultComboBoxModel<Integer>(new Integer[] { 0, 1, 2, 3, 4, 5 }));

        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jLabel6.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(alsoReverseComplementCheckBox1, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.alsoReverseComplementCheckBox1.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel7, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jLabel7.text")); // NOI18N

        outputFolderTextField.setText(org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.outputFolderTextField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(selectOutputFolderButton, org.openide.util.NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.selectOutputFolderButton.text")); // NOI18N
        selectOutputFolderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectOutputFolderButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(probeNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 168, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(66, 66, 66)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(outputFolderTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(sequenceCollectionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(overallGapNumberSelectionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(threePrimeGapNumberSelectionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(alsoReverseComplementCheckBox1))
                                        .addGap(0, 0, Short.MAX_VALUE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(263, 263, 263)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(selectSequenceCollectionButton)
                    .addComponent(selectOutputFolderButton))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(probeNameLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(selectSequenceCollectionButton))
                    .addComponent(sequenceCollectionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(overallGapNumberSelectionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(threePrimeGapNumberSelectionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(alsoReverseComplementCheckBox1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(outputFolderTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(selectOutputFolderButton))
                .addContainerGap(37, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void selectSequenceCollectionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectSequenceCollectionButtonActionPerformed

        JFileChooser fc = new JFileChooser();
        String ttText = NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jfilechooser.tooltiptext");
        String dialogTitle = NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jfilechooser.dialogtitle");
        fc.setToolTipText(ttText);
        fc.setDialogTitle(dialogTitle);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileFilter ff = new FileFilter(){

            @Override
            public boolean accept(File f) {
                String n = f.getName();
                //if( n.endsWith(".fa") )
                return true;
            }

            @Override
            public String getDescription() {
                return NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jfilechooser.description");
            }
            
        };
        fc.addChoosableFileFilter(ff);
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == 0) {
            File primerDir = fc.getSelectedFile();
            this.sequenceCollectionTextField.setText(primerDir.getAbsolutePath());
        }
        
    }//GEN-LAST:event_selectSequenceCollectionButtonActionPerformed

    private void selectOutputFolderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectOutputFolderButtonActionPerformed
        JFileChooser fc = new JFileChooser();
        String ttText = NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jfilechooser2.tooltiptext");
        String dialogTitle = NbBundle.getMessage(ProbeMatcherActionVisualPanel1.class, "ProbeMatcherActionVisualPanel1.jfilechooser2.dialogtitle");
        fc.setToolTipText(ttText);
        fc.setDialogTitle(dialogTitle);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == 0) {
            File outputDir = fc.getSelectedFile();
            this.outputFolderTextField.setText(outputDir.getAbsolutePath());
        }
    }//GEN-LAST:event_selectOutputFolderButtonActionPerformed

    private void sequenceCollectionTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sequenceCollectionTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sequenceCollectionTextFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox alsoReverseComplementCheckBox1;
    private javax.swing.JComboBox<ProbeMatcher> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField outputFolderTextField;
    private javax.swing.JComboBox<Integer> overallGapNumberSelectionCombo;
    private javax.swing.JLabel probeNameLabel;
    private javax.swing.JButton selectOutputFolderButton;
    private javax.swing.JButton selectSequenceCollectionButton;
    private javax.swing.JTextField sequenceCollectionTextField;
    private javax.swing.JComboBox<Integer> threePrimeGapNumberSelectionCombo;
    // End of variables declaration//GEN-END:variables

    @Override
    public void insertUpdate(DocumentEvent e) {
        processDocumentEvent(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        processDocumentEvent(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        processDocumentEvent(e);
    }
    
    private void processDocumentEvent(DocumentEvent e){
        if(sequenceCollectionTextField.getDocument() == e.getDocument()){
            firePropertyChange(PROP_SEQUENCE_SET, 0, 1);
        }
        if(outputFolderTextField.getDocument() == e.getDocument()){
            firePropertyChange(PROP_OUTPUT_FOLDER, 0, 1);
        }
    }
}
