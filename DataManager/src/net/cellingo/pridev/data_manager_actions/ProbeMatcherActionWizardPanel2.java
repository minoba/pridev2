/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_actions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import net.cellingo.pridev.data_manager.file_type_primer.PriDataObject;
import net.cellingo.pridev.probe_matcher_api.AnalysisMode;
import net.cellingo.pridev.taxonomy_archive.NcbiTaxonomyArchiveWrapper;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

/**
 *
 * @author Gebruiker
 */
public class ProbeMatcherActionWizardPanel2 implements WizardDescriptor.Panel<WizardDescriptor>, PropertyChangeListener{
    private boolean isValid = false;
    private final EventListenerList listeners = new EventListenerList();
    private WizardDescriptor wiz = null;
    private ProbeMatcherActionVisualPanel2 component;
    private final PriDataObject firstPrimer;

    ProbeMatcherActionWizardPanel2(PriDataObject firstPrimer) {
        this.firstPrimer = firstPrimer;
    }
    
        // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public ProbeMatcherActionVisualPanel2 getComponent() {
        if (component == null) {
            component = new ProbeMatcherActionVisualPanel2();
            //component.addPropertyChangeListener(this);
        }
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        //return true;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
        //
        return checkValidity();
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        listeners.add(ChangeListener.class, l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        listeners.remove(ChangeListener.class, l);
    }
    
    protected final void fireChangeEvent(Object source, boolean oldState, boolean newState){
        if(oldState != newState){
            ChangeEvent ev = new ChangeEvent(source);
            for (ChangeListener listener : listeners.getListeners(ChangeListener.class)){
                listener.stateChanged(ev);
            }
        }
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        this.wiz = wiz;
        getComponent().addPropertyChangeListener(this);
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        wiz.putProperty(ProbeMatcherActionVisualPanel2.PROP_ANALYSIS_MODE, getComponent().getAnalysisMode());
        wiz.putProperty(ProbeMatcherActionVisualPanel2.PROP_SECOND_PRIMER, getComponent().getSecondPrimerFile());
        wiz.putProperty(ProbeMatcherActionVisualPanel2.PROP_MAX_DISTANCE, getComponent().getMaximumDistance());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        boolean oldState = isValid;
        isValid = checkValidity();
        fireChangeEvent(this, oldState, isValid);
    }
    
    private boolean checkValidity(){
        if( NcbiTaxonomyArchiveWrapper.getDefault() == null ){
            setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel1.errormessage.no_ncbi_archive"));
            return false;
        }
        AnalysisMode analysisMode = getComponent().getAnalysisMode();
        if(analysisMode == AnalysisMode.PRIMER_PAIR){
            String secondPrimer = getComponent().getSecondPrimerFile();
            if(secondPrimer.length() == 0){
                setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.second_primer_file1"));
                return false;
            }
            File secondPrimerFile = new File(secondPrimer);
            if(! (secondPrimerFile.exists() && secondPrimerFile.canRead() && secondPrimerFile.isFile() )){
                setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.second_primer_file2"));
                return false;
            }
            FileObject pf = FileUtil.toFileObject(secondPrimerFile);
            try {
                PriDataObject secondPrimerObject = (PriDataObject) DataObject.find(pf);//(pf, null);
                if( this.firstPrimer.isForwardStrandTargeted() &&  secondPrimerObject.isForwardStrandTargeted() ){
                    setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.second_primer_file4"));
                    return false;
                }
                if( ! this.firstPrimer.isForwardStrandTargeted() &&  ! secondPrimerObject.isForwardStrandTargeted() ){
                    setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.second_primer_file5"));
                    return false;
                }
            } catch (DataObjectNotFoundException ex) {
                setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.second_primer_file3"));
                return false;
            } catch( ClassCastException ex ){
                setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.second_primer_file3"));
                return false;
            }
            
        }
        try{
            int maxDistance = getComponent().getMaximumDistance();
            if (maxDistance < 1 ){
                setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.maximum_distance2"));
                return false;
            }
        }catch(NumberFormatException ex){
            setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel2.class, "ProbeMatcherActionWizardPanel2.errormessage.maximum_distance"));
            return false;
        }
        setMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel1.class, "ProbeMatcherActionWizardPanel1.errormessage.input_valid"));
        return true;
    }
    
    private void setMessage(String message){
        wiz.getNotificationLineSupport().setInformationMessage(message);
    }
    private void setErrorMessage(String message){
        wiz.getNotificationLineSupport().setErrorMessage(message);
    }


}
