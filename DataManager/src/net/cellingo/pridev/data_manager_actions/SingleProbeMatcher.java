/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.swing.JComponent;
import javax.swing.SwingWorker;
import net.cellingo.pridev.data_manager.file_type_primer.PriDataObject;
import net.cellingo.pridev.data_manager.utils.FileUtils;
import net.cellingo.pridev.probe_matcher_api.AnalysisMode;
import net.cellingo.pridev.probe_matcher_api.Probe;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchResults;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchResultsWriter;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchResultsWriterXMLimpl;
import net.cellingo.pridev.probe_matcher_api.ProbeMatcher;
import net.cellingo.pridev.probe_matcher_api.ProbeMatcherStringConstants;
import net.cellingo.pridev.taxonomy_archive.NcbiTaxonomyArchiveWrapper;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "Tools",
        id = "net.cellingo.pridev.data_manager_actions.SingleProbeMatcher")
@ActionRegistration(
        iconBase = "net/cellingo/pridev/data_manager_actions/tree_highlight_primer.png",
        displayName = "#CTL_SingleProbeMatcher")
@ActionReferences({
    @ActionReference(path = "Menu/Tools", position = 0, separatorAfter = 50),
    @ActionReference(path = "Toolbars/File", position = 350),
    @ActionReference(path = "Loaders/text/x-probe/Actions", position = 137)
})
@Messages("CTL_SingleProbeMatcher=Match probe")
public final class SingleProbeMatcher implements ActionListener {

    private final PriDataObject context;

    public SingleProbeMatcher(PriDataObject context) {
        this.context = context;

    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        List<WizardDescriptor.Panel<WizardDescriptor>> panels = new ArrayList<WizardDescriptor.Panel<WizardDescriptor>>();
        ProbeMatcherActionWizardPanel2 wp2 = new ProbeMatcherActionWizardPanel2(this.context);
        panels.add(new ProbeMatcherActionWizardPanel1());
        panels.add(wp2);
        
        String[] steps = new String[panels.size()];
        for (int i = 0; i < panels.size(); i++) {
            Component c = panels.get(i).getComponent();
            // Default step name to component name of panel.
            steps[i] = c.getName();
            if (c instanceof JComponent) { // assume Swing components
                JComponent jc = (JComponent) c;
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_SELECTED_INDEX, i);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, steps);
                jc.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, true);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DISPLAYED, true);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_NUMBERED, true);
            }
        }
        WizardDescriptor wiz = new WizardDescriptor(new WizardDescriptor.ArrayIterator<WizardDescriptor>(panels));
        wiz.setTitleFormat(new MessageFormat("{0}"));
        String title = org.openide.util.NbBundle.getMessage(SingleProbeMatcher.class, "SingleProbeMatcher.title.text");
        wiz.setTitle(title);
        if (DialogDisplayer.getDefault().notify(wiz) == WizardDescriptor.FINISH_OPTION) {
            
            Map<String, Object> props = wiz.getProperties();
            
            ProbeMatcher pm = (ProbeMatcher) props.get(ProbeMatcherActionVisualPanel1.PROP_ALGORITHM);
            String sequenceSet = (String) props.get(ProbeMatcherActionVisualPanel1.PROP_SEQUENCE_SET);
            int maxMismatches = (Integer) props.get(ProbeMatcherActionVisualPanel1.PROP_MAX_MISMATCHES_OVERALL);
            int maxMismatches3Prime = (Integer) props.get(ProbeMatcherActionVisualPanel1.PROP_MAX_MISMATCHES_3_PRIME);
            boolean alsoReverseComplement = (Boolean) props.get(ProbeMatcherActionVisualPanel1.PROP_ALSO_REVERSE_COMPLEMENT);
            String outputFolder = (String) props.get(ProbeMatcherActionVisualPanel1.PROP_OUTPUT_FOLDER);
            AnalysisMode analysisMode = (AnalysisMode) props.get(ProbeMatcherActionVisualPanel2.PROP_ANALYSIS_MODE);
            //System.out.println("outputFolder" + outputFolder);
            //String outputFilePrefix = outputFolder + File.separator + ;
            String extension = "xml";
            String outputFile = null;
            int maxDistance = 0;
            Probe rvProbe = null;
            if( analysisMode == AnalysisMode.PRIMER_PAIR ){
                try {
                    String secondPrimerFile = (String) props.get(ProbeMatcherActionVisualPanel2.PROP_SECOND_PRIMER);
                    FileObject pf = FileUtil.toFileObject(new File(secondPrimerFile));
                    PriDataObject secondPrimerObject = (PriDataObject) DataObject.find(pf);
                    rvProbe = secondPrimerObject.getProbeRepresentation();
                    maxDistance = (Integer) props.get(ProbeMatcherActionVisualPanel2.PROP_MAX_DISTANCE);
                    //outputFile = outputFolder + File.separator + this.context.getPrimerName() + "_" + secondPrimerObject.getPrimerName() + "_pair_matching_.xml";
                    String fileBaseName = this.context.getPrimerName() + "_" + secondPrimerObject.getPrimerName() + "_pair_matching";
                    outputFile = FileUtils.generateOutputFileSerial(outputFolder, fileBaseName, extension);
                    //System.out.println("writing to " + outputFile);

                } catch (DataObjectNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                    return;
                }
            }else{
                String fileBaseName = this.context.getPrimerName() + "_matching";
                outputFile = FileUtils.generateOutputFileSerial(outputFolder, fileBaseName, extension);
                //System.out.println("writing to " + outputFile);
                
            }
            
            Probe fwProbe = context.getProbeRepresentation();
//            System.out.println(
//                    "\nsequenceSet=" + sequenceSet + 
//                    "\n; maxMismatches=" + maxMismatches + 
//                    "\n; maxMismatches3Prime=" + maxMismatches3Prime + 
//                    "\n; alsoReverseComplement=" + alsoReverseComplement +
//                    "\n; rvProbe=" + rvProbe +
//                    "\n; fwProbe=" + fwProbe +
//                    "\n; maxDistance=" + maxDistance +
//                    "\n; outputFile=" + outputFile +
//                    "\n; analysisMode=" + analysisMode);
            SwingWorker<Void, Void> worker = createSwingWorker(analysisMode, pm, sequenceSet, fwProbe, rvProbe, maxMismatches, maxMismatches3Prime, alsoReverseComplement, maxDistance, outputFile);
            worker.execute();
            //System.out.println("End probe matching init");
        }

    }

    /**
     * creates the SwingWorker to execute the probe matching in the background
     * @param matcher
     * @param sequenceSet
     * @param fwProbe
     * @param maxMismatches
     * @param maxMismatches3Prime
     * @return swingWorker
     */
    private SwingWorker<Void, Void> createSwingWorker(
            final AnalysisMode analysisMode,
            final ProbeMatcher matcher, 
            final String sequenceSet, 
            final Probe fwProbe, 
            final Probe rvProbe, 
            final int maxMismatches, 
            final int maxMismatches3Prime, 
            final boolean alsoReverseComplement,
            final int maxDistance,
            final String outputFile) {
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                try{
                    Probe fwProbeClone = fwProbe.clone();
                    ProgressHandle p = ProgressHandleFactory.createHandle("Matching probe " + fwProbe.getName() + " vs " + sequenceSet);
                    p.start();
                    ProbeMatchResults probeMatchResults;
                    if(analysisMode == AnalysisMode.SINGLE_PROBE){
                        //System.out.println(">>>>>>>>> matching 1 probe");
                        probeMatchResults = matcher.matchSingleProbe(
                            NcbiTaxonomyArchiveWrapper.getDefault(), 
                            sequenceSet, 
                            fwProbeClone, 
                            maxMismatches, 
                            maxMismatches3Prime, 
                            alsoReverseComplement);
                    }
                    else{
                        //System.out.println(">>>>>>>>>>> matching 2 primers:"
//                                + "\n sequenceSet="+ sequenceSet
//                                + "\n fwProbe=" + fwProbeClone
//                                + "\n rvProbeClone=" + rvProbeClone
//                                + "\n maxMismatches=" + maxMismatches
//                                + "\n maxMismatches3Prime="+ maxMismatches3Prime
//                                + "\n maxDistance="+ maxDistance
//                                + "\n alsoReverseComplement=" + alsoReverseComplement);
                        Probe rvProbeClone = rvProbe.clone();
                        probeMatchResults = matcher.matchPrimerPair(
                            NcbiTaxonomyArchiveWrapper.getDefault(), 
                            sequenceSet, 
                            fwProbeClone, 
                            rvProbeClone,
                            maxMismatches, 
                            maxMismatches3Prime, 
                            maxDistance, 
                            alsoReverseComplement);
                    }
                    //System.out.println("!!! writing !!!");
                    ProbeMatchResultsWriter writer = new ProbeMatchResultsWriterXMLimpl(outputFile);
                    Properties props = new Properties();
                    props.put(ProbeMatcherStringConstants.ANALYSIS_MODE, analysisMode.toString());
                    props.put(ProbeMatcherStringConstants.SEQUENCE_FILE_PATH, sequenceSet);
                    props.put(ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH, NcbiTaxonomyArchiveWrapper.getArchiveFile().getAbsolutePath());
                    props.put(ProbeMatcherStringConstants.FORWARD_PROBE_NAME, fwProbe.getName());
                    props.put(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE, fwProbe.getSequence());

                    if (analysisMode == AnalysisMode.PRIMER_PAIR) {
                        //System.out.println("PRIMER_PAIR");
                        props.put(ProbeMatcherStringConstants.REVERSE_PROBE_NAME, rvProbe.getName());
                        props.put(ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE, rvProbe.getSequence());
                        props.put(ProbeMatcherStringConstants.MAX_DISTANCE, Integer.toString(maxDistance));

                    }
                    props.put(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT, Boolean.toString(alsoReverseComplement));
                    StringBuilder targetTaxIDs = new StringBuilder();
                    for(Integer tid : fwProbe.getTargetNodeIds() ){
                        targetTaxIDs.append(tid.toString());
                        targetTaxIDs.append(",");
                    }
                    targetTaxIDs.deleteCharAt(targetTaxIDs.length()-1);
                    props.put(ProbeMatcherStringConstants.TARGET_TAX_IDS, targetTaxIDs.toString());

                    props.put(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES, Integer.toString(maxMismatches));
                    props.put(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME, Integer.toString(maxMismatches3Prime));
                    props.put(ProbeMatcherStringConstants.THREE_PRIME_END_SIZE, Integer.toString(5));

                    //System.out.println("[00000000] " + props);
                    //System.out.println("[1111] " + AnalysisMode.valueOf(props.getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE)));

                    writer.write(probeMatchResults, props);


                    p.finish();
                }catch(Exception e){
                    e.printStackTrace();;
                }
                return null;
                
            }

            @Override
            protected void done() {
                StatusDisplayer.getDefault().setStatusText("Probe matching finished!");
            }
        };
        return worker;
//        return null;
    }
}
