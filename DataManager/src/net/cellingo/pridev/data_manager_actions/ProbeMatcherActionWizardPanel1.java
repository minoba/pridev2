/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_actions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import net.cellingo.pridev.taxonomy_archive.NcbiTaxonomyArchiveWrapper;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

/**
 *
 * @author Gebruiker
 */
public class ProbeMatcherActionWizardPanel1 implements WizardDescriptor.Panel<WizardDescriptor>, PropertyChangeListener{
    private boolean isValid = false;
    private final EventListenerList listeners = new EventListenerList();
    private WizardDescriptor wiz = null;
    private ProbeMatcherActionVisualPanel1 component;
    
    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public ProbeMatcherActionVisualPanel1 getComponent() {
        if (component == null) {
            component = new ProbeMatcherActionVisualPanel1();
            //component.addPropertyChangeListener(this);
        }
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        //return true;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
        //
        return checkValidity();
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        listeners.add(ChangeListener.class, l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        listeners.remove(ChangeListener.class, l);
    }
    
    protected final void fireChangeEvent(Object source, boolean oldState, boolean newState){
        if(oldState != newState){
            ChangeEvent ev = new ChangeEvent(source);
            for (ChangeListener listener : listeners.getListeners(ChangeListener.class)){
                listener.stateChanged(ev);
            }
        }
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        this.wiz = wiz;
        getComponent().addPropertyChangeListener(this);
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        wiz.putProperty(ProbeMatcherActionVisualPanel1.PROP_ALGORITHM, getComponent().getProbeMatcherAlgorithm());
        wiz.putProperty(ProbeMatcherActionVisualPanel1.PROP_SEQUENCE_SET, getComponent().getSequenceSetFile());
        wiz.putProperty(ProbeMatcherActionVisualPanel1.PROP_MAX_MISMATCHES_OVERALL, getComponent().getMaximumMismatchesOverall());
        wiz.putProperty(ProbeMatcherActionVisualPanel1.PROP_MAX_MISMATCHES_3_PRIME, getComponent().getMaximumMismatches3PrimeEnd());
        wiz.putProperty(ProbeMatcherActionVisualPanel1.PROP_ALSO_REVERSE_COMPLEMENT, getComponent().getAlsoReverseComplement());
        wiz.putProperty(ProbeMatcherActionVisualPanel1.PROP_OUTPUT_FOLDER, getComponent().getOutputFolder());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        boolean oldState = isValid;
        isValid = checkValidity();
        fireChangeEvent(this, oldState, isValid);
    }
    
    private boolean checkValidity(){
        if( NcbiTaxonomyArchiveWrapper.getDefault() == null ){
            setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel1.class, "ProbeMatcherActionWizardPanel1.errormessage.no_ncbi_archive"));
            return false;
        }
        File seqSet = new File(getComponent().getSequenceSetFile());
        if(getComponent().getSequenceSetFile().length() == 0){
            setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel1.class, "ProbeMatcherActionWizardPanel1.errormessage.sequence_set1"));
            return false;
        }
        if(! (seqSet.exists() && seqSet.canRead() && seqSet.isFile() )){
            setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel1.class, "ProbeMatcherActionWizardPanel1.errormessage.sequence_set2"));
        }
        String outputFolderStr = getComponent().getOutputFolder();
        File outputFolder = new File(outputFolderStr);
        if(outputFolderStr.length() == 0){
            setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel1.class, "ProbeMatcherActionWizardPanel1.errormessage.output_folder1"));
            return false;
        }
        if(! (outputFolder.exists() && outputFolder.canWrite() && outputFolder.isDirectory())){
            setErrorMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel1.class, "ProbeMatcherActionWizardPanel1.errormessage.output_folder2"));
            return false;
        }

        setMessage(NbBundle.getMessage(ProbeMatcherActionWizardPanel1.class, "ProbeMatcherActionWizardPanel1.errormessage.input_valid"));
        return true;
    }
    
    private void setMessage(String message){
        wiz.getNotificationLineSupport().setInformationMessage(message);
    }
    private void setErrorMessage(String message){
        wiz.getNotificationLineSupport().setErrorMessage(message);
    }


}
