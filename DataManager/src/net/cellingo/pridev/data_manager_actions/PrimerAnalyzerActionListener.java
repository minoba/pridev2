/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager_actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import net.cellingo.pridev.data_manager.file_type_primer.PriDataObject;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "Tools",
        id = "net.cellingo.pridev.data_manager_actions.PrimerAnalyzerActionListener")
@ActionRegistration(
        iconBase = "net/cellingo/pridev/data_manager_actions/SEO-icon.png",
        displayName = "#CTL_PrimerAnalyzerActionListener")
@ActionReferences({
    @ActionReference(path = "Toolbars/File", position = 352),
    @ActionReference(path = "Loaders/text/x-probe/Actions", position = 150, separatorBefore = 125)
})
@Messages("CTL_PrimerAnalyzerActionListener=Evaluate primer properties")
public final class PrimerAnalyzerActionListener implements ActionListener {

    private final PriDataObject context;

    public PrimerAnalyzerActionListener(PriDataObject context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        FileObject f = context.getPrimaryFile();
        String displayName = FileUtil.getFileDisplayName(f);
        String msg = "I am " + displayName + ". Hear me roar!";
        NotifyDescriptor nd = new NotifyDescriptor.Message(msg);
        DialogDisplayer.getDefault().notify(nd);
    }
}
