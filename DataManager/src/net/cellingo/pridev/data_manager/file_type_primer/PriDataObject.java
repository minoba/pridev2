/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.file_type_primer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import net.cellingo.pridev.probe_matcher_api.Probe;
import nl.bioinf.noback.taxonomy.model.TaxNode;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@Messages({
    "LBL_Pri_LOADER=Files of Pri"
})
@MIMEResolver.ExtensionRegistration(
        displayName = "#LBL_Pri_LOADER",
        mimeType = "text/x-probe",
        extension = {"pri"})
@DataObject.Registration(
        mimeType = "text/x-probe",
        iconBase = "net/cellingo/pridev/data_manager/file_type_primer/probe.png",
        displayName = "#LBL_Pri_LOADER",
        position = 300)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300),
    @ActionReference(
            path = "Loaders/text/x-probe/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400)
})
public class PriDataObject extends MultiDataObject {

    private Properties properties;
    public static final String SEQUENCE = "sequence";
    public static final String PRIMER_ORIENTATION = "primer_orientation";
    public static final String TARGET_TAXIDS = "target_taxids";
    public static final String REF = "ref";
    public static final String DESCRIPTION = "description";

    public PriDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor("text/x-probe", true);
        this.properties = new Properties();
        properties.load(new FileInputStream(FileUtil.toFile(pf)));

        getPrimaryFile().addFileChangeListener(new FileChangeAdapter() {
            @Override
            public void fileChanged(FileEvent fe) {
                try {
                    properties.load(new FileInputStream(FileUtil.toFile(getPrimaryFile())));
                    firePropertyChange(null, null, null);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        });
    }

    
    public static PriDataObject createNew(String name, String folder, String sequence, String strand, String description, String xRef, List<TaxNode> targetTaxNodes) throws DataObjectExistsException, IOException {
        //System.out.println("new pimer will be created from " + name + " / " + folder + " / " + sequence + " / " + strand + " / " + description + " / " + xRef + " / " + targetTaxNodes);
        String path = (folder.endsWith(File.separator) ? folder : folder + File.separator);
        path += (name + ".pri");
        File priFile = new File(path);
        StringBuilder targetTaxIDs = new StringBuilder();
        for(TaxNode tn : targetTaxNodes){
            targetTaxIDs.append(tn.getTaxID());
            targetTaxIDs.append(",");
        }
        targetTaxIDs.deleteCharAt(targetTaxIDs.length()-1);
        try {
            PrintWriter out = new PrintWriter(priFile);
            out.println(SEQUENCE + "=" + sequence);
            out.println(PRIMER_ORIENTATION + "=" + strand);
            out.println(TARGET_TAXIDS + "=" + targetTaxIDs.toString());
            out.println(REF + "=" + xRef);
            out.println(DESCRIPTION + "=" + description);
            out.close();
        } catch (FileNotFoundException ex) {
            throw new IOException(ex);
        }
        FileObject pf = FileUtil.toFileObject(priFile);
        return (PriDataObject) DataObject.find(pf);//(pf, null);
    }
    
    public String getPrimerName() {
        return getPrimaryFile().getName();
    }

    public String getPrimerDescription() {
        return properties.getProperty(DESCRIPTION);
    }

    public void setPrimerDescription(String description) {
        properties.setProperty(DESCRIPTION, description);
    }

    public String getPrimerSequence() {
        return properties.getProperty(SEQUENCE);
    }

    public void setPrimerSequence(String seq) {
        properties.setProperty(SEQUENCE, seq);
    }

    public String getPrimerTargetTaxIDs() {
        return properties.getProperty(TARGET_TAXIDS);
    }

    public void setPrimerTargetTaxIDs(String targetTaxID) {
        properties.setProperty(TARGET_TAXIDS, targetTaxID);
    }

    public String getPrimerTargetStrand() {
        return properties.getProperty(PRIMER_ORIENTATION);
    }

    public void setPrimerTargetStrand(String targetStrand) {
        properties.setProperty(PRIMER_ORIENTATION, targetStrand);
    }

    public boolean isForwardStrandTargeted(){
        return this.getPrimerTargetStrand().toLowerCase().contains("forward");
    }
    
    public String getPrimerReference() {
        return properties.getProperty(REF);
    }

    public void setPrimerReference(String reference) {
        properties.setProperty(REF, reference);
    }
    
    /**
     * creates a Probe object representing this PriDataObject
     * @return probe
     */
    public Probe getProbeRepresentation(){
        String[] targetIDstrings = getPrimerTargetTaxIDs().split(",");
        List<Integer> targetIDs = new ArrayList<Integer>();
        for( String tid : targetIDstrings ){
            targetIDs.add(Integer.parseInt(tid));
        }
        Probe p = new Probe(getPrimerName(), getPrimerSequence(), isForwardStrandTargeted(), targetIDs );
        return p;
    }

        @Override
    protected Node createNodeDelegate() {
        return new DataNode(this, Children.create(new PriChildFactory(this), true), getLookup());
    }

    private static class PriChildFactory extends ChildFactory<String> {

        private final PriDataObject dObj;

        public PriChildFactory(PriDataObject dObj) {
            this.dObj = dObj;
        }

        @SuppressWarnings({"rawtypes", "unchecked"})
        @Override
        protected boolean createKeys(List list) {
            FileObject fObj = dObj.getPrimaryFile();
            try {
                List<String> dObjContent = fObj.asLines();
                
                list.addAll(dObjContent);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
            return true;
        }

        @Override
        protected Node createNodeForKey(String key) {
            Node childNode = new AbstractNode(Children.LEAF);
            childNode.setDisplayName(key);
            return childNode;
        }
    }

    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_Pri_EDITOR",
            iconBase = "net/cellingo/pridev/data_manager/file_type_primer/probe.png",
            mimeType = "text/x-probe",
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "Pri",
            position = 1000)
    @Messages("LBL_Pri_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }
}
