sequence=<the sequence of the primer/probe here>
name=<the name of the primer/probe here>
target_taxids=<the NCBI tax IDs of the primer/probe targets here, separated by commas>
primer_orientation=<the orientastion of the primer/probe here; is mainly important for integration in primer pairs>
ref=<a PubMed or other reference inserted here>
