/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.file_type_primer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import net.cellingo.pridev.taxonomy_archive.NcbiTaxonomyArchiveWrapper;
import net.cellingo.pridev.taxonomy_swing_utils.TaxNodeSelectionTableModel;
import nl.bioinf.noback.taxonomy.model.TaxNode;
import nl.bioinf.noback.taxonomy.model.TaxTree;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.awt.UndoRedo;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileRenameEvent;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@MultiViewElement.Registration(
        displayName = "#LBL_Pri_VISUAL",
        iconBase = "net/cellingo/pridev/data_manager/file_type_primer/probe.png",
        mimeType = "text/x-probe",
        persistenceType = TopComponent.PERSISTENCE_NEVER,
        preferredID = "PriVisual",
        position = 100)
@Messages("LBL_Pri_VISUAL=Visual")
public final class PriVisualElement extends JPanel implements MultiViewElement, FileChangeListener, TableModelListener {
    public static final String PROP_SELECTED_TARGETS = "selected_targets";

    private JToolBar toolbar = new JToolBar();
    private transient MultiViewElementCallback callback;
    private List<TaxNode> targetTaxNodes = new ArrayList<TaxNode>();
    private TaxNodeSelectionTableModel tableModel = new TaxNodeSelectionTableModel(new ArrayList<TaxNode>());
    private final String[] targetIDs;

    private PriDataObject priDataObject;

    public PriVisualElement(Lookup lkp) {
        priDataObject = lkp.lookup(PriDataObject.class);
        assert priDataObject != null;
        initComponents();
        
        priDataObject.getPrimaryFile().addFileChangeListener(this);
        showPrimerInfo();

        //TODO THE pROPERTYcHANGElISTENER MAY NOT BE THE BEST TOOL FOR THIS JOB
        priDataObject.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                showPrimerInfo();
            }
        });
        TableColumn column = null;
        for (int i = 0; i < 5; i++) {
            column = this.jTable1.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(20); 
            } else if (i==1){
                column.setPreferredWidth(75);
            } else if (i==2){
                column.setPreferredWidth(300);
            } else if (i==3){
                column.setPreferredWidth(100);
            }else{
                column.setPreferredWidth(100);
            }
        }
        this.tableModel.addTableModelListener(this);
        this.targetIDs = priDataObject.getPrimerTargetTaxIDs().split(",");
        
        //swingworker to retreive taxnodes from NCBI taxonomy.
        SwingWorker<List<TaxNode>, Void> worker = new SwingWorker<List<TaxNode>, Void>(){

            @Override
            public List<TaxNode> doInBackground() throws Exception {
                List<TaxNode> innerTargets = new ArrayList<TaxNode>();
                int attempts = 0;
                TaxTree tree = null;
                
                while(tree == null){
                    //System.out.println("attempt " + attempts);
                    if(attempts > 15) return null;
                    attempts++;
                    Thread.sleep(1000);
                    tree = NcbiTaxonomyArchiveWrapper.getDefault();
                    //System.out.println("tree = " + tree);
                }
                for( String targetID : targetIDs ){
                    innerTargets.add(NcbiTaxonomyArchiveWrapper.getDefault().getNode(Integer.parseInt(targetID)));
                }
                return innerTargets;
            }
            
            @Override
            public void done(){
                try {
                    targetTaxNodes = get();
                    if( targetTaxNodes == null ){
                        Notification noti = NotificationDisplayer.getDefault().notify(
                        "Could not get TaxIDs from NSBI repo",
                        ImageUtilities.loadImageIcon("net/cellingo/pridev/taxonomy_explorer/info.png", true), "", null);

                        System.out.println("error");
                        return;
                    }
                    tableModel.addtaxNodes(targetTaxNodes);
                    tableModel.selectAll();
                } catch (InterruptedException ex) {
                    Exceptions.printStackTrace(ex);
                } catch (ExecutionException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        };
        try {
            worker.execute();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    private void showPrimerInfo(){
        org.openide.awt.Mnemonics.setLocalizedText(priTitleLabel, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.priTitleLabel.text"));
        this.priTitleLabel.setText(this.priTitleLabel.getText() + " \"" + priDataObject.getPrimerName() + "\"");
        this.priDescriptionTextField.setText( priDataObject.getPrimerDescription());
        this.priSequenceTextField.setText( priDataObject.getPrimerSequence() );
        this.targetStrandTextField.setText( priDataObject.getPrimerTargetStrand() );
        this.referenceTextField.setText( priDataObject.getPrimerReference() );
    }

    @Override
    public String getName() {
        return "PriVisualElement";
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        priTitleLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        priSequenceTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        priDescriptionTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        targetStrandTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        referenceTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.jLabel3.text")); // NOI18N

        priTitleLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(priTitleLabel, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.priTitleLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.jLabel2.text")); // NOI18N

        priSequenceTextField.setText(org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.priSequenceTextField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.jLabel1.text")); // NOI18N

        priDescriptionTextField.setText(org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.priDescriptionTextField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.jLabel4.text")); // NOI18N

        targetStrandTextField.setText(org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.targetStrandTextField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.jLabel5.text")); // NOI18N

        referenceTextField.setText(org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.referenceTextField.text")); // NOI18N

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(PriVisualElement.class, "PriVisualElement.jLabel6.text")); // NOI18N

        jTable1.setModel(tableModel);
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(priTitleLabel)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(priSequenceTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(targetStrandTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                .addComponent(referenceTextField))
                            .addComponent(priDescriptionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel6)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(priTitleLabel)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(priSequenceTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(priDescriptionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(targetStrandTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(referenceTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField priDescriptionTextField;
    private javax.swing.JTextField priSequenceTextField;
    private javax.swing.JLabel priTitleLabel;
    private javax.swing.JTextField referenceTextField;
    private javax.swing.JTextField targetStrandTextField;
    // End of variables declaration//GEN-END:variables
    @Override
    public JComponent getVisualRepresentation() {
        return this;
    }

    @Override
    public JComponent getToolbarRepresentation() {
        return toolbar;
    }

    @Override
    public Action[] getActions() {
        return new Action[0];
    }

    @Override
    public Lookup getLookup() {
        return priDataObject.getLookup();
    }

    @Override
    public void componentOpened() {
    }

    @Override
    public void componentClosed() {
    }

    @Override
    public void componentShowing() {
    }

    @Override
    public void componentHidden() {
    }

    @Override
    public void componentActivated() {
    }

    @Override
    public void componentDeactivated() {
    }

    @Override
    public UndoRedo getUndoRedo() {
        return UndoRedo.NONE;
    }

    @Override
    public void setMultiViewCallback(MultiViewElementCallback callback) {
        this.callback = callback;
        callback.getTopComponent().setDisplayName(this.priDataObject.getName());
    }

    @Override
    public CloseOperationState canCloseElement() {
        return CloseOperationState.STATE_OK;
    }
    
        @Override
    public void fileFolderCreated(FileEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fileDataCreated(FileEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fileChanged(FileEvent fe) {
        this.showPrimerInfo();
    }

    @Override
    public void fileDeleted(FileEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fileRenamed(FileRenameEvent fe) {
        this.showPrimerInfo();
    }

    @Override
    public void fileAttributeChanged(FileAttributeEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        //firePropertyChange(PROP_SELECTED_TARGETS, 0, 1);
        
        this.targetTaxNodes = tableModel.getSelectedNodes();
        System.out.println("PRIVISUALELEMENT should process changed selection of target nodes");
    }
    

}
