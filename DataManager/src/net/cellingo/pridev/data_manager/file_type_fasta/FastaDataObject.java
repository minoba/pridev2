/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.file_type_fasta;

import java.io.IOException;
import java.util.List;
import net.cellingo.sequence_tools.io.SequenceReader;
import net.cellingo.sequence_tools.io.UnknownSequenceFormatException;
import net.cellingo.sequence_tools.sequences.BiologicalSequence;
import net.cellingo.sequence_tools.sequences.SequenceCreationException;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@Messages({
    "LBL_Fasta_LOADER=Files of Fasta"
})
@MIMEResolver.ExtensionRegistration(
        displayName = "#LBL_Fasta_LOADER",
        mimeType = "text/x-fasta",
        extension = {"fa", "fas", "fna"})
@DataObject.Registration(
        mimeType = "text/x-fasta",
        iconBase = "net/cellingo/pridev/data_manager/file_type_fasta/sequences_file.png",
        displayName = "#LBL_Fasta_LOADER",
        position = 300)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300),
    @ActionReference(
            path = "Loaders/text/x-fasta/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400)
})
public class FastaDataObject extends MultiDataObject {
    private List<BiologicalSequence> sequences;
    public FastaDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor("text/x-fasta", true);
        
    }

    public List<BiologicalSequence> getSequenceList() throws UnknownSequenceFormatException, SequenceCreationException{
        if(sequences == null){
            loadSequences();
        }
        return sequences;
    }
    
    public void loadSequences() throws UnknownSequenceFormatException, SequenceCreationException{
        SequenceReader reader = new SequenceReader(FileUtil.toFile(getPrimaryFile()) );
        this.sequences = reader.readMultiFasta();
    }
    
    public int getSequenceCount() throws UnknownSequenceFormatException, SequenceCreationException{
        SequenceReader reader = new SequenceReader(FileUtil.toFile(getPrimaryFile()) );
        List<BiologicalSequence> tempSequences = reader.readMultiFasta();
        return tempSequences.size();
    }
    
    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_Fasta_EDITOR",
            iconBase = "net/cellingo/pridev/data_manager/file_type_fasta/sequences_file.png",
            mimeType = "text/x-fasta",
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "Fasta",
            position = 1000)
    @Messages("LBL_Fasta_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }
}
