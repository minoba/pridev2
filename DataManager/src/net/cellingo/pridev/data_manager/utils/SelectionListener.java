/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.utils;

import net.cellingo.pridev.probe_matcher_api.MatchStatisticSelection;

/**
 *
 * @author Gebruiker
 */
public interface SelectionListener {
    public void selectionMade(int targetID, MatchStatisticSelection selection);
}
