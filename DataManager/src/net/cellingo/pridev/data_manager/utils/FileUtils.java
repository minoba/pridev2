/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.utils;

import java.io.File;

/**
 *
 * @author Gebruiker
 */
public class FileUtils {
    /**
     * utility function generating a new file name using a serial number if the file with the given base already exists
     * @param outputFolder the folder
     * @param fileBaseName the base name
     * @param fileExtension the file extension, without dot
     * @return 
     */
    public static String generateOutputFileSerial(String outputFolder, String fileBaseName, String fileExtension){
        String base = outputFolder + File.separator + fileBaseName;
        File proposed = new File(base + "." + fileExtension);
        int serial = 1;
        boolean fileExists = proposed.exists();
        while(fileExists){
            proposed = new File(base + "_[" + serial + "]." + fileExtension);
            serial++;
            fileExists = proposed.exists();
        }
        return proposed.getAbsolutePath();
    }
}
