/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.file_type_probe_eval;

import javax.swing.table.AbstractTableModel;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchStatistics;

/**
 *
 * @author Gebruiker
 */
public class TargetGroupProbeMatchingResultsTable extends AbstractTableModel {

    private final ProbeMatchStatistics matchStatistics;

    public TargetGroupProbeMatchingResultsTable(ProbeMatchStatistics matchStatistics) {
        this.matchStatistics = matchStatistics;
    }

    @Override
    public int getRowCount() {
        return 14;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return null;
//        switch (rowIndex) {
//            case 0: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.target_group_species_count.toString();
//                } else {
//                    return matchStatistics.getTargetGroupSpeciesNumber();
//                }
//            }
//            case 1: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.target_group_matches_forward.toString();
//                } else {
//                    return matchStatistics.getTargetGroupMatchesFW();
//                }
//            }
//            case 2: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.unique_target_group_matches_forward.toString();
//                } else {
//                    return "NA";//matchStatistics.getMatchedTargetTaxNodesFW().size();
//                }
//            }
//            case 3: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.non_target_group_matches_forward.toString();
//                } else {
//                    return matchStatistics.getNonTargetGroupMatchesFW();
//                }
//            }
//            case 4: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.unique_non_target_group_matches_forward.toString();
//                } else {
//                    return "NA";
//                }
//            }
//            case 5: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.target_group_matches_reverse.toString();
//                } else {
//                    return matchStatistics.getTargetGroupMatchesRV();
//                }
//            }
//            case 6: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.unique_target_group_matches_reverse.toString();
//                } else {
//                    return "NA";
//                }
//            }
//
//            case 7: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.non_target_group_matches_reverse.toString();
//                } else {
//                    return matchStatistics.getNonTargetGroupMatchesRV();
//                }
//            }
//            case 8: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.unique_non_target_group_matches_reverse.toString();
//                } else {
//                    return "NA";
//                }
//            }
//
//            case 9: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.target_group_matches_both.toString();
//                } else {
//                    return matchStatistics.getTargetGroupMatchesBoth();
//                }
//            }
//            case 10: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.unique_target_group_matches_both.toString();
//                } else {
//                    return "NA";
//                }
//            }
//
//            case 11: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.non_target_group_matches_both.toString();
//                } else {
//                    return matchStatistics.getNonTargetGroupMatchesBoth();
//                }
//            }
//            case 12: {
//                if (columnIndex == 0) {
//                    return ProbeMatchingProperty.unique_non_target_group_matches_both.toString();
//                } else {
//                    return "NA";
//                }
//            }
//            default:
//                return "NA";
//        }
    }
    @Override
    public String getColumnName(int columnIndex) {
        if(columnIndex == 0) return "Property";
        else return "Value";
    }

}
