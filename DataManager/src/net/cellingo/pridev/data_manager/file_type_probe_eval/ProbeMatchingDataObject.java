/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.file_type_probe_eval;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import net.cellingo.pridev.probe_matcher_api.AnalysisMode;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchResults;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchResultsReader;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchResultsReaderXMLimpl;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchStatistics;
import net.cellingo.pridev.probe_matcher_api.ProbeMatcherStringConstants;
import net.cellingo.pridev.probe_matching_api.beans.FragmentSizes;
import net.cellingo.pridev.probe_matcher_api.MatchStatisticSelection;
import net.cellingo.pridev.probe_matcher_api.TaxNodeMatchResults;
import net.cellingo.pridev.probe_matching_api.beans.TaxNodeMatches;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

@Messages({
    "LBL_ProbeMatching_LOADER=Files of ProbeMatching"
})
@MIMEResolver.NamespaceRegistration(
        displayName = "#LBL_ProbeMatching_LOADER",
        mimeType = "text/probe_match_data+xml",
        elementNS = {"probe_match_data"})
@DataObject.Registration(
        mimeType = "text/probe_match_data+xml",
        iconBase = "net/cellingo/pridev/data_manager/file_type_probe_eval/tree_highlight_primer.png",
        displayName = "#LBL_ProbeMatching_LOADER",
        position = 300)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300),
    @ActionReference(
            path = "Loaders/text/probe_match_data+xml/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400)
})
public class ProbeMatchingDataObject extends MultiDataObject {
    private static final String XPATH_BASE = "/root/probe_match_data/";
    public static final String XPATH_SUBTREE_ANALYSIS_SETTINGS = "analysis_settings/";
    public static final String XPATH_SUBTREE_METADATA = "meta_data/";
    public static final String XPATH_SUBTREE_MATCH_STATISTICS = "match_statistics/";
    public static final String XPATH_SUBTREE_MATCH_STATISTIC = "match_statistics/match_statistic";
    public static final String XPATH_SUBTREE_SEQUENCE_REPRESENTED_NODES = "sequence_represented_nodes/";
    
    
    private Document xmlDoc;
    private XPath xpath;
    private List<Integer> targetTaxIDs;
    private AnalysisMode analysisMode;
    private ProbeMatchResults probeMatchResults;
    
    public ProbeMatchingDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor("text/probe_match_data+xml", true);
        
        loadFile();
    }
    
    private void loadFile(){
        try {
            FileObject pf = getPrimaryFile();
            
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            this.xmlDoc = db.parse(FileUtil.toFile(pf));
            xmlDoc.getDocumentElement().normalize();
            XPathFactory xPathfactory = XPathFactory.newInstance();
            this.xpath = xPathfactory.newXPath();
            
            String targetIDsStr = getProperty(ProbeMatcherStringConstants.TARGET_TAX_IDS, XPATH_SUBTREE_ANALYSIS_SETTINGS);
            String[] targetIDsArr = targetIDsStr.split(",");
            List<Integer> targetTaxIDs = new ArrayList<Integer>();
            for(String id : targetIDsArr ){
                targetTaxIDs.add(Integer.parseInt(id));
            }
            this.targetTaxIDs = targetTaxIDs;

            String analysisModeStr = getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE, XPATH_SUBTREE_ANALYSIS_SETTINGS);
            this.analysisMode = AnalysisMode.valueOf(analysisModeStr);
        } catch (ParserConfigurationException ex) {
            Exceptions.printStackTrace(ex);
        } catch (SAXException ex) {
            Exceptions.printStackTrace(ex);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } 
    }

    /**
     * returns the target taxIDs associated with this data object
     * @return targetTaxIDs
     */
    public List<Integer> getTargetTaxIDs() {
        return targetTaxIDs;
    }

    /**
     * returns the analysismode
     * @return analysisMode
     */
    public AnalysisMode getAnalysisMode() {
        return analysisMode;
    }
    

    /**
     * returns a simple String property value for the given key
     * @param property
     * @param documentSubTree 
     * @return stringValue
     */
    public String getProperty(String property, String documentSubTree) {
        try {
            String expression = XPATH_BASE + documentSubTree + property;
            //System.out.println("fetching " + expression);
            XPathExpression expr = xpath.compile(expression);
            String result = expr.evaluate(xmlDoc);
            return result;
        } catch (XPathExpressionException ex) {
            return "NA";
        }
    }

        /**
     * returns a simple String property value for the given key
     * @param property
     * @param documentSubTree 
     * @param targetGroupID the target_group_id is used as attribute selector
     * @return stringValue
     */
    public String getProperty(String property, String documentSubTree, int targetGroupID) {
        try {
            String expression = XPATH_BASE + documentSubTree + "[@" + ProbeMatcherStringConstants.TARGET_GROUP_ID + "='" + targetGroupID + "']/" + property;
            //System.out.println("############# expression=" + expression);
            XPathExpression expr = xpath.compile(expression);
            String result = expr.evaluate(xmlDoc);
            return result;
        } catch (XPathExpressionException ex) {
            return "NA";
        }
    }
    
    private void readXMLfile(){
        ProbeMatchResultsReader reader = new ProbeMatchResultsReaderXMLimpl();
        //System.out.println("opening " + this.getPrimaryFile().getPath());
        try {
            this.probeMatchResults = reader.read(this.getPrimaryFile().getPath());
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        if(probeMatchResults == null ){
            Exceptions.printStackTrace( new IOException("Error reading file object") );
        }
    }
    
    public FragmentSizes getFragmentSizes(int taxID){
        if(probeMatchResults == null) readXMLfile();
        ProbeMatchStatistics pms = probeMatchResults.getProbeMatchStatistics(taxID);
        FragmentSizes fs = new FragmentSizes(taxID);
        fs.setTruePositiveFragmentSizes( pms.getTargetGroupsFragmentSizes() );
        fs.setFalsePositiveFragmentSizes( pms.getNonTargetGroupsFragmentSizes() );
        return fs;
    }
    
    public TaxNodeMatches getTaxNodeMatches(int targetID, MatchStatisticSelection selection) {
        if(probeMatchResults == null) readXMLfile();
        ProbeMatchStatistics probeMatchStatistics = probeMatchResults.getProbeMatchStatistics(targetID);
        
        TaxNodeMatches tnm = new TaxNodeMatches(selection);
        List<TaxNodeMatchResults> taxNodeMatchResults = probeMatchStatistics.getTaxNodeMatchResults(selection);
        Comparator<TaxNodeMatchResults> comp = null;
        if(analysisMode == AnalysisMode.PRIMER_PAIR){
            comp = new Comparator<TaxNodeMatchResults>() {
                @Override
                public int compare(TaxNodeMatchResults o1, TaxNodeMatchResults o2) {
                    return o2.getBothStrandMatchCount() - o1.getBothStrandMatchCount();
                }
            };
        }
        else{
            comp = new Comparator<TaxNodeMatchResults>() {
                @Override
                public int compare(TaxNodeMatchResults o1, TaxNodeMatchResults o2) {
                    return o2.getForwardStrandMatchCount() - o1.getForwardStrandMatchCount();
                }
            };
        }
        
        
        
        Collections.sort(taxNodeMatchResults, comp);

        tnm.setTaxNodeMatches( taxNodeMatchResults );
        
        
        tnm.setAnalysisMode(analysisMode);
        return tnm;
    }
        
    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_ProbeMatching_EDITOR",
            iconBase = "net/cellingo/pridev/data_manager/file_type_probe_eval/tree_highlight_primer.png",
            mimeType = "text/probe_match_data+xml",
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "ProbeMatching",
            position = 1000)
    @Messages("LBL_ProbeMatching_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }


    
}
