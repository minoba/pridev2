/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.file_type_probe_eval;

import java.text.NumberFormat;
import javax.swing.table.AbstractTableModel;
import net.cellingo.pridev.probe_matcher_api.ProbeMatcherStringConstants;
import org.openide.util.NbBundle;

/**
 *
 * @author Gebruiker
 */
public class ProbeMatchingResultsTableModel extends AbstractTableModel{
    private final ProbeMatchingDataObject dataObject;
    private final NumberFormat numberFormat;

    public ProbeMatchingResultsTableModel(ProbeMatchingDataObject dataObject){
        this.dataObject = dataObject;
        this.numberFormat = NumberFormat.getInstance();
    }
    
    @Override
    public int getRowCount() {
        return 14;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(rowIndex){
            case 0: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.TIME_STAMP);
                else return dataObject.getProperty(ProbeMatcherStringConstants.TIME_STAMP, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 1: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.ANALYSIS_MODE);
                else return dataObject.getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 2: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.FORWARD_PROBE_NAME);
                else return dataObject.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_NAME, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 3: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE);
                else return dataObject.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 4: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.REVERSE_PROBE_NAME);
                else return dataObject.getProperty(ProbeMatcherStringConstants.REVERSE_PROBE_NAME, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 5: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE);
                else return dataObject.getProperty(ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 6: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.SEQUENCE_FILE_PATH);
                else return dataObject.getProperty(ProbeMatcherStringConstants.SEQUENCE_FILE_PATH, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 7: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH);
                else return dataObject.getProperty(ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 8: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES);
                else return dataObject.getProperty(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 9: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME);
                else return dataObject.getProperty(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 10: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.TARGET_TAX_IDS);
                else return dataObject.getProperty(ProbeMatcherStringConstants.TARGET_TAX_IDS, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 11: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.MAX_DISTANCE);
                else return dataObject.getProperty(ProbeMatcherStringConstants.MAX_DISTANCE, ProbeMatchingDataObject.XPATH_SUBTREE_ANALYSIS_SETTINGS);
            }
            case 12: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.TREE_SIZE);
                else{
                    String c = dataObject.getProperty(ProbeMatcherStringConstants.TREE_SIZE, ProbeMatchingDataObject.XPATH_SUBTREE_METADATA);
                    return numberFormat.format(Integer.parseInt(c));
                }
            }
            case 13: {
                if( columnIndex == 0 ) return NbBundle.getMessage(ProbeMatchingResultsTableModel.class, "ProbeMatchingResultsTableModel." + ProbeMatcherStringConstants.SEQUENCE_COUNT);
                else{
                    String c = dataObject.getProperty(ProbeMatcherStringConstants.SEQUENCE_COUNT, ProbeMatchingDataObject.XPATH_SUBTREE_METADATA);
                    return numberFormat.format(Integer.parseInt(c));
                }
            }
            

            default: return "NA";
        }
        //return "Hi";
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        if(columnIndex == 0) return "Property";
        else return "Value";
    }

//    @Override
//    public boolean isCellEditable(int rowIndex, int columnIndex) {
//        return false;
//    }
}
