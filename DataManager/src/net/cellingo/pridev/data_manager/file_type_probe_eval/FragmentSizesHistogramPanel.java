/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cellingo.pridev.data_manager.file_type_probe_eval;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.util.Arrays;
import java.util.HashMap;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;

/**
 *
 * @author Gebruiker
 */
public class FragmentSizesHistogramPanel extends JPanel {
    public FragmentSizesHistogramPanel(HashMap<Integer, Integer> truePositiveOccurrences, HashMap<Integer, Integer> falsePositiveOccurrences){
        HashMap<Integer, Integer> tp = createTP();
        HashMap<Integer, Integer> fp = createFP();

        JFreeChart chart = createChart(truePositiveOccurrences, falsePositiveOccurrences, "Fragment sizes", "Size", "Occurrences");
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(390, 250));
        chartPanel.setMinimumSize(new java.awt.Dimension(390, 250));
        
        //this.setPreferredSize(new java.awt.Dimension(500, 270));
        //this.setMinimumSize(new java.awt.Dimension(500, 270));
        this.setBackground(Color.LIGHT_GRAY);
        this.setLayout(new BorderLayout());
        this.add(chartPanel, BorderLayout.CENTER);//
    }
    
    private DataSeries fillDataSeries(HashMap<Integer, Integer> fragmentSizeOccurrences){
        DataSeries s = new DataSeries(); 
        s.minimum = Integer.MAX_VALUE;
        s.maximum = Integer.MIN_VALUE;
        int sumOfOccurrences = 0;
        for(Integer size : fragmentSizeOccurrences.keySet()){
            if(size < s.minimum) s.minimum = size;
            if(size > s.maximum) s.maximum = size;
            sumOfOccurrences += fragmentSizeOccurrences.get(size);
            
        }
        s.series = new double[sumOfOccurrences];
        int i=0;
        for(Integer size : fragmentSizeOccurrences.keySet()){
            int count = fragmentSizeOccurrences.get(size);
            for(int j=0; j<count; j++){
                
                //System.out.println("adding " + size + " at index " + (i));
                s.series[i] = size;
                i++;
            }
        }
        //System.out.println("s.series= " + Arrays.toString(s.series));
        return s;
    }
    
    
    private JFreeChart createChart(
            HashMap<Integer, Integer> truePositiveFragments, 
            HashMap<Integer, Integer> falsePositiveFragments, 
            String title, 
            String xAxisLabel,
            String yAxisLabel){
        
        DataSeries v1 = fillDataSeries(truePositiveFragments);
        DataSeries v2 = fillDataSeries(falsePositiveFragments);
               
        int absMin = v1.minimum <= v2.minimum ? v1.minimum : v2.minimum;
        int absMax = v1.maximum >= v2.maximum ? v1.maximum : v2.maximum;
        absMin -= 2;
        absMax += 3;
        HistogramDataset dataset = new HistogramDataset();
        int bin = absMax - absMin ;
        //int x = 42;
//        absMin -= 2;
//        absMax += 2;
//        SimpleHistogramDataset d1 = new SimpleHistogramDataset("key");
//        //SimpleHistogramDataset d2 = new SimpleHistogramDataset("key");
//        for(int i = 1;i <= bin; i++){
//            double start = (double)absMin +i;
//            double end = (double)(absMin + i + 1);
//            SimpleHistogramBin b = new SimpleHistogramBin(start, end, true, false );
//            System.out.println("adding bin=" + start+ " - " + end );
//            d1.addBin(b);
//            //d2.addBin(b);
//        }
//        
//        
//        d1.addObservations(v1.series);
//        d1.addObservations(v2.series);
                
        //System.out.println("absMin="+ absMin + "; absMax" + absMax + " bin=" + bin);
        dataset.addSeries("True Positives", v1.series, bin, absMin, absMax);
        dataset.addSeries("False Positives", v2.series, bin, absMin, absMax);

        boolean legend = true;
        boolean tooltips = false;
        boolean urls = true;
        JFreeChart chart = ChartFactory.createHistogram(title, xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL, legend, tooltips, urls);
        chart.setBackgroundPaint(new Color(230, 230, 230));
        XYPlot xyplot = (XYPlot) chart.getPlot();
        xyplot.setForegroundAlpha(0.7F);
        xyplot.setBackgroundPaint(Color.WHITE);
        xyplot.setDomainGridlinePaint(new Color(150, 150, 150));
        xyplot.setRangeGridlinePaint(new Color(150, 150, 150));
        XYBarRenderer xybarrenderer = (XYBarRenderer) xyplot.getRenderer();
        xybarrenderer.setMargin(0.1);
        //xybarrenderer.setMargin(bin);
        xybarrenderer.setShadowVisible(false);
        xybarrenderer.setBarPainter(new StandardXYBarPainter());
//    xybarrenderer.setDrawBarOutline(false);
        return chart;
    }

    private HashMap<Integer, Integer> createTP() {
        HashMap<Integer, Integer> tp = new HashMap<Integer, Integer>();
        tp.put(397, 1);
        tp.put(398, 5);
        tp.put(399, 10);
        tp.put(400, 8);
        tp.put(401, 7);
        tp.put(405, 2);
        tp.put(410, 1);
        return tp;
    }

    private HashMap<Integer, Integer> createFP() {
        HashMap<Integer, Integer> fp = new HashMap<Integer, Integer>();
        fp.put(381, 1);
        fp.put(391, 2);
        fp.put(393, 3);
        fp.put(398, 11);
        fp.put(399, 4);
        fp.put(400, 2);
        fp.put(401, 1);
        return fp;
    }
    
    private class DataSeries{
        int minimum;
        int maximum;
        double[] series;
    }
}
