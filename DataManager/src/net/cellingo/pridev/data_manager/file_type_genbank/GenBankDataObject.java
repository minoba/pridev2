/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.data_manager.file_type_genbank;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.cellingo.sequence_tools.annotation.SequenceObject;
import net.cellingo.sequence_tools.io.SequenceReader;
import net.cellingo.sequence_tools.io.SequenceReaderListener;
import net.cellingo.sequence_tools.io.UnknownSequenceFormatException;
import net.cellingo.sequence_tools.sequences.BiologicalSequence;
import net.cellingo.sequence_tools.sequences.SequenceCreationException;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@Messages({
    "LBL_GenBank_LOADER=Files of GenBank"
})
@MIMEResolver.ExtensionRegistration(
        displayName = "#LBL_GenBank_LOADER",
        mimeType = "text/x-genbank",
        extension = {"gb", "gen"})
@DataObject.Registration(
        mimeType = "text/x-genbank",
        iconBase = "net/cellingo/pridev/data_manager/file_type_genbank/genbank.png",
        displayName = "#LBL_GenBank_LOADER",
        position = 300)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300),
    @ActionReference(
            path = "Loaders/text/x-genbank/Actions",
            id =
            @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400)
})
public class GenBankDataObject extends MultiDataObject implements SequenceReaderListener {
    private List<SequenceObject> sequences;
    
    public GenBankDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor("text/x-genbank", true);
    }
    
    public List<SequenceObject> getSequenceList() throws UnknownSequenceFormatException, SequenceCreationException{
        if(sequences == null){
            sequences = new ArrayList<SequenceObject>();
            loadSequences();
        }
        return sequences;
    }
    
    public void loadSequences() throws UnknownSequenceFormatException, SequenceCreationException{
        SequenceReader reader = new SequenceReader(FileUtil.toFile(getPrimaryFile()) );
        reader.addSequenceReaderListener(this);
        reader.read();
    }
    
    public int getSequenceCount() throws UnknownSequenceFormatException, SequenceCreationException{
        SequenceReader reader = new SequenceReader(FileUtil.toFile(getPrimaryFile()) );
        SequenceCounter sc = new SequenceCounter();
        reader.addSequenceReaderListener(sc);
        reader.read();
        return sc.getSequenceCount();
    }
  

    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_GenBank_EDITOR",
            iconBase = "net/cellingo/pridev/data_manager/file_type_genbank/genbank.png",
            mimeType = "text/x-genbank",
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "GenBank",
            position = 1000)
    @Messages("LBL_GenBank_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }

    @Override
    public void sequenceRead(SequenceObject so) {
        sequences.add(so);
    }

    @Override
    public void sequenceReadingFinished() {
        System.out.println("[GenBankDataObject.class] FINISHED READING GENBANK");
    }
    
    private class SequenceCounter implements SequenceReaderListener{
        private int counter = 0;
        
        public int getSequenceCount(){
            return counter;
        }
        
        @Override
        public void sequenceRead(SequenceObject so) {
            counter++;
        }

        @Override
        public void sequenceReadingFinished() {
            
            //void implementation
        }
    }
}
