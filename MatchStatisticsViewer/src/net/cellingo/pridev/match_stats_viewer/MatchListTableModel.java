/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cellingo.pridev.match_stats_viewer;

import java.text.NumberFormat;
import javax.swing.table.AbstractTableModel;
import net.cellingo.pridev.probe_matcher_api.AnalysisMode;
import net.cellingo.pridev.probe_matcher_api.TaxNodeMatchResults;
import net.cellingo.pridev.probe_matching_api.beans.TaxNodeMatches;
import net.cellingo.pridev.taxonomy_archive.NcbiTaxonomyArchiveWrapper;
import nl.bioinf.noback.taxonomy.model.TaxNode;


/**
 *
 * @author Gebruiker
 */
public class MatchListTableModel extends AbstractTableModel{
    private static final String[] columnNamesSingleProbe = {"Name", "Tax ID", "matches" };
    private static final String[] columnNamesPrimerPair = {"Name", "Tax ID", "FW primer matches", "RV primer matches", "Combi matches" };
    
    private final TaxNodeMatches matches;
    private final NumberFormat numberFormat;
    private String[] columnNames;

    public MatchListTableModel(TaxNodeMatches matches){
        this.matches = matches;
        this.numberFormat = NumberFormat.getInstance();
        
        if(matches != null){
            if(matches.getAnalysisMode() == AnalysisMode.PRIMER_PAIR ){
                columnNames = columnNamesPrimerPair;
            }else{
                columnNames = columnNamesSingleProbe;
            }
        }else{
            columnNames = columnNamesSingleProbe;
        }

    }
    
    @Override
    public int getRowCount() {
        if(matches == null ) return 0;
        else return matches.getTaxNodeMatches().size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TaxNodeMatchResults tnmr = matches.getTaxNodeMatches().get(rowIndex);
        String name = "NA";
        try{
            TaxNode node = NcbiTaxonomyArchiveWrapper.getDefault().getNode( tnmr.getTaxID() );
            name = node.getScientificName();
        }catch(Exception e){ }
        if(matches == null) return "NA";
        
        if(matches.getAnalysisMode() == AnalysisMode.PRIMER_PAIR ){
            switch(columnIndex){
                case 0: return name;
                case 1: return tnmr.getTaxID();
                case 2: return tnmr.getForwardStrandMatchCount();
                case 3: return tnmr.getReverseStrandMatchCount();
                case 4: return tnmr.getBothStrandMatchCount();
                default: return "NA";
            }
        }else{
            switch(columnIndex){
                case 0: return name;
                case 1: return tnmr.getTaxID();
                case 2: return tnmr.getForwardStrandMatchCount();
                default: return "NA";
            }
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

//    @Override
//    public boolean isCellEditable(int rowIndex, int columnIndex) {
//        return false;
//    }
}
