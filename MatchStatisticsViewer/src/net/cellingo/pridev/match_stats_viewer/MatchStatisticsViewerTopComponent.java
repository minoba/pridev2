/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.match_stats_viewer;

import java.util.Collection;
import javax.swing.SwingUtilities;
import net.cellingo.pridev.probe_matching_api.beans.TaxNodeMatches;
import net.cellingo.pridev.utilities.CentralLookup;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;
import org.openide.util.Utilities;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
        dtd = "-//net.cellingo.pridev.match_stats_viewer//MatchStatisticsViewer//EN",
        autostore = false
)
@TopComponent.Description(
        preferredID = "MatchStatisticsViewerTopComponent",
        //iconBase="SET/PATH/TO/ICON/HERE", 
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "properties", openAtStartup = true)
@ActionID(category = "Window", id = "net.cellingo.pridev.match_stats_viewer.MatchStatisticsViewerTopComponent")
@ActionReference(path = "Menu/Window" , position = 333 )
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_MatchStatisticsViewerAction",
        preferredID = "MatchStatisticsViewerTopComponent"
)
@Messages({
    "CTL_MatchStatisticsViewerAction=MatchStatisticsViewer",
    "CTL_MatchStatisticsViewerTopComponent=MatchStatisticsViewer Window",
    "HINT_MatchStatisticsViewerTopComponent=This is a MatchStatisticsViewer window"
})
public final class MatchStatisticsViewerTopComponent extends TopComponent implements LookupListener{
    private Lookup.Result<TaxNodeMatches> result = null;
    private String baseTitle;
    private TaxNodeMatches matches;
    
    public MatchStatisticsViewerTopComponent() {
        initComponents();
        this.baseTitle = titleLabel.getText();
        setName(Bundle.CTL_MatchStatisticsViewerTopComponent());
        setToolTipText(Bundle.HINT_MatchStatisticsViewerTopComponent());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(titleLabel, org.openide.util.NbBundle.getMessage(MatchStatisticsViewerTopComponent.class, "MatchStatisticsViewerTopComponent.titleLabel.text")); // NOI18N

        jTable1.setModel(new MatchListTableModel(matches));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(titleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
    @Override
    public void componentOpened() {
        //result = Utilities.actionsGlobalContext().lookupResult(TaxNodeMatches.class);
        //result.addLookupListener(this);
        result = CentralLookup.getDefault().lookupResult(TaxNodeMatches.class);
        result.addLookupListener(this);
    }

    @Override
    public void componentClosed() {
        //result.removeLookupListener(this);
        result.removeLookupListener(this);
    }

    
//    @Override
//    public void addNotify() {
//        //super.addNotify(); //To change body of generated methods, choose Tools | Templates.
//        result = CentralLookup.getDefault().lookupResult(TaxNodeMatches.class);
//        result.addLookupListener(this);
//    }
//
//    @Override
//    public void removeNotify() {
//        //super.removeNotify(); //To change body of generated methods, choose Tools | Templates.
//        result.removeLookupListener(this);
//    }
//    
    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    public void resultChanged(LookupEvent le) {
        //result.allInstances().iterator().next();
        Collection<? extends TaxNodeMatches> allEvents = result.allInstances();

        if (!allEvents.isEmpty()) {
            //System.out.println("[" + this.getClass().getSimpleName() + "]" + " LIST SIZE=" + allEvents.size());
            //TaxNodeMatches matches = allEvents.iterator().next();
            this.matches = allEvents.iterator().next();
            this.jTable1.setModel(new MatchListTableModel(matches));
            this.jTable1.getColumnModel().getColumn(0).setMinWidth(200);
            CentralLookup.getDefault().remove(matches);
            titleLabel.setText(baseTitle + "; type=" + matches.getMatchStatisticSelection());
        } else {
            titleLabel.setText(baseTitle + "[no selection]");
        }
        SwingUtilities.invokeLater(
            new Runnable() {
                @Override
                public void run() {
                    repaint();
                }
            }
        );

    }
}
