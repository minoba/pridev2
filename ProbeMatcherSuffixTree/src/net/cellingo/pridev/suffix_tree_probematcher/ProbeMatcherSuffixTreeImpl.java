/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.suffix_tree_probematcher;

import java.io.IOException;
import net.cellingo.pridev.probe_matcher_api.Probe;
import net.cellingo.pridev.probe_matcher_api.ProbeMatchResults;
import net.cellingo.pridev.probe_matcher_api.ProbeMatcher;
import nl.bioinf.noback.taxonomy.model.TaxTree;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Gebruiker
 */
@ServiceProvider(service=ProbeMatcher.class)
public class ProbeMatcherSuffixTreeImpl implements ProbeMatcher{
    
    @Override
    public String toString(){
        return "Suffix Tree matching algorithm";
    }

    @Override
    public ProbeMatchResults matchSingleProbe(
            TaxTree taxTree, 
            String sequenceFileName, 
            Probe probe, 
            int maxAllowedMismatches, 
            int maxMismatches3PrimeEnd,
            boolean alsoReverseComplement) throws IOException{
                //System.out.println("!!!!!! matching probe single probe !!!!");
                SuffixTreeProbeMatcher probeMatcher = new SuffixTreeProbeMatcher();
                ProbeMatchResults probeMatchResults = probeMatcher.matchProbe( 
                        taxTree, sequenceFileName, probe, maxAllowedMismatches, maxMismatches3PrimeEnd, alsoReverseComplement);
                return probeMatchResults;
    }
    
    @Override
    public ProbeMatchResults matchPrimerPair(
            TaxTree taxTree, 
            String sequenceFileName, 
            Probe probeFW, 
            Probe probeRV, 
            int maxAllowedMismatches, 
            int maxMismatches3PrimeEnd, 
            int maxDistance,
            boolean alsoReverseComplement) throws IOException{
                //System.out.println("############# matching primer pair");
                SuffixTreeProbeMatcher probeMatcher = new SuffixTreeProbeMatcher();
                //taxTree, sequenceFileName, probeFW, probeRV, maxAllowedMismatches, maxMismatches3PrimeEnd, maxDistance, alsoReverseComplement)
                ProbeMatchResults probeMatchResults = probeMatcher.matchPrimerPair(
                        taxTree, sequenceFileName, probeFW, probeRV, maxAllowedMismatches, maxMismatches3PrimeEnd, maxDistance, alsoReverseComplement);
                return probeMatchResults;
        }
    
}
